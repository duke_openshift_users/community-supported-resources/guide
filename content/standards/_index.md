+++
title = "Project Standards"
description = "Some standards exist for working with projects in the DOUG community supported resources group, at both the project level and for specific types of projects."
+++

## Project-level Standards

*   README.md - Each project should contain a README.md describing the project and linking to the DOUG Community Supported Resources website.
*   CONTRIBUTING.md - Projects should contain a boilerplate CONTRIBUTING.md linking to this page.  Project specific guidelines can be included as well.
*   MAINTAINERS.md - Projects should contain a MAINTAINERS.md file, listing the responsible maintainers in `Name - <email@address>` format.

## Container Image Standards

Specific guidelines for container image projects include:

*   Container images should be built using Gitlab CI, and run through some level of testing and validation before being pushed to the registry.
*   The build process should inject container images with the following labels:
    *   `git_repository_url`
    *   `git_commit`
