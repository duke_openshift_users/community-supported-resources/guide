+++
title = "Contributing"
description = "We welcome anyone who is interested in maintaining container, Kubernetes or OKD/OpenShift resources with the community."
+++

## Contributing to a Project

Contributing to an existing project is as easy as forking the project, making your changes, and issuing a merge request.  After a merge request has been created, the project maintainers will review it before merging.

Some projects may have their own additional contribution guidelines.  Check the `CONTRIBUTING.md` file for any project-specific guidelines.

## Maintain a New Project

If you would like to maintain a new project, contact Nate Childers at [nate.childers@duke.edu](mailto:nate.childers@duke.edu) to discuss what you'd like to manage and have a repository created for you.
